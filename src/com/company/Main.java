interface Cipher {

    /** deszyfrowanie wiadomosci*/
    String decode(final String message);

    /** szyfrowanie wiadomosci */
    String encode(final String message);
}

class AtBashCipher implements Cipher {

}

class Main {

    public static void main(String[] args) {

        Cipher atBashCipher = new AtBashCipher();

        // Encode: Zoz nz plgz
        System.out.println("Encode: " + atBashCipher.encode("Ala ma kota"));

        // Decode: Ala ma kota
        System.out.println("Decode: " + atBashCipher.decode("Zoz nz plgz"));
    }
}